# ASP.NET Core Web API with DTOs and AutoMapper

This repository contains a starter ASP.NET Core Web API project, serving as a practical ground for implementing modern API development techniques. The focus is on using Data Transfer Objects (DTOs), AutoMapper, and resolving circular reference issues in API responses.

## Getting Started

This project guides you through enhancing a basic Web API with advanced features for robust and efficient data handling.

### Prerequisites

- .NET 6.0 SDK
- Visual Studio 2019 or later
- Understanding of ASP.NET Core, Entity Framework Core, and RESTful principles

### Setup

1. Clone this repository.
2. Open the solution file (`*.sln`) in Visual Studio.
3. Restore any missing NuGet packages.

## Project Structure

- `Models`: `Product` entity with a one-to-many relationship to `Review`.
- `Repositories`: Includes `IProductRepository` and `ProductRepository`.
- `Services`: Contains `IProductService` and `ProductService` with basic logic.
- `Controllers`: `ProductsController` implemented with direct entity usage.
- `DTOs`: (To be implemented) For Data Transfer Objects.
- `MappingProfile`: (To be implemented) AutoMapper configuration.

## Activity Objective

Enhance the API by:
- Introducing DTOs for data shaping and circular reference resolution.
- Configuring and using AutoMapper for object mapping.
- Refactoring service and controller layers for DTO usage.
- Testing API functionality with Postman.

## Contributing

This project is intended for learning. Feel free to fork, explore, and enhance.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
