﻿namespace Noroff.Samples.ASPNET.DTOs.Data.Entities
{
    public class Review
    {
        public int Id { get; set; }
        public string Author { get; set; }
        public string Content { get; set; }
        public int ProductId { get; set; }

        // Navigation property
        public Product Product { get; set; }
    }

}
