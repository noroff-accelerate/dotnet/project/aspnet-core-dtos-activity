﻿namespace Noroff.Samples.ASPNET.DTOs.Data.Entities
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }

        // Navigation property
        public ICollection<Review> Reviews { get; set; }
    }

}
