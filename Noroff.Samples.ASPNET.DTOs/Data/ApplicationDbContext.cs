﻿using Microsoft.EntityFrameworkCore;
using Noroff.Samples.ASPNET.DTOs.Data.Entities;

namespace Noroff.Samples.ASPNET.DTOs.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        public DbSet<Product> Products { get; set; }
        public DbSet<Review> Reviews { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Seed data
            modelBuilder.Entity<Product>().HasData(
                new Product { Id = 1, Name = "Laptop", Description = "A high-end laptop", Price = 1500.00M },
                new Product { Id = 2, Name = "Smartphone", Description = "Latest model smartphone", Price = 800.00M }
            );

            modelBuilder.Entity<Review>().HasData(
                new Review { Id = 1, ProductId = 1, Author = "John Doe", Content = "Great laptop, very fast." },
                new Review { Id = 2, ProductId = 1, Author = "Jane Smith", Content = "Good value for the price." },
                new Review { Id = 3, ProductId = 2, Author = "Emily White", Content = "Battery life could be better." }
            );
        }
    }

}
