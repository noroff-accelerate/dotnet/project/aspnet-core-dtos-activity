﻿using Noroff.Samples.ASPNET.DTOs.Data.Entities;

namespace Noroff.Samples.ASPNET.DTOs.Services
{
    public interface IProductService
    {
        Task<IEnumerable<Product>> GetAllProductsAsync();
        Task<Product> GetProductByIdAsync(int productId);
        Task AddProductAsync(Product product);
        Task UpdateProductAsync(Product product);
        Task DeleteProductAsync(int productId);
    }

}
