﻿using Noroff.Samples.ASPNET.DTOs.Data.Entities;
using Noroff.Samples.ASPNET.DTOs.Repositories;

namespace Noroff.Samples.ASPNET.DTOs.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _repository;

        public ProductService(IProductRepository repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<Product>> GetAllProductsAsync()
        {
            return await _repository.GetAllProductsAsync();
        }

        public async Task<Product> GetProductByIdAsync(int productId)
        {
            return await _repository.GetProductByIdAsync(productId);
        }

        public async Task AddProductAsync(Product product)
        {
            if (string.IsNullOrEmpty(product.Name))
            {
                throw new ArgumentException("Product name cannot be empty.");
            }

            if (product.Price <= 0)
            {
                throw new ArgumentException("Product price must be greater than zero.");
            }

            await _repository.AddProductAsync(product);
        }

        public async Task UpdateProductAsync(Product product)
        {
            var existingProduct = await _repository.GetProductByIdAsync(product.Id);
            if (existingProduct == null)
            {
                throw new KeyNotFoundException("Product not found.");
            }

            await _repository.UpdateProductAsync(product);
        }

        public async Task DeleteProductAsync(int productId)
        {
            var product = await _repository.GetProductByIdAsync(productId);
            if (product == null)
            {
                throw new KeyNotFoundException("Product to delete not found.");
            }

            await _repository.DeleteProductAsync(productId);
        }
    }

}
